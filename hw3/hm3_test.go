package hm3

import "testing"

func TestWork(t *testing.T) {

	testStrings := map[string]string{
		"abdas":    "abdas",
		`a4bc2d5e`: `aaaabccddddde`,
		`45`:       "",
		`qwe\4\5`:  "qwe45",
		`qwe\45`:   "qwe44444",
		`qwe\\5`:   `qwe\\\\\`,
		`ыфв2`:     `ыфвв`,
	}

	// hasError := false

	for str, res := range testStrings {
		fncRes := Work(str)
		if fncRes != res {
			t.Fatalf(`Excepted "%s", got %s `, fncRes, res)
			// hasError = true
		}
	}

	// if !hasError {
	// 	println("SUCCESS!!!")
	// }

}
