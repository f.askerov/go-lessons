package hm3

import (
	"strconv"
	"strings"
)

const (
	slashRune rune = 92
	zeroRune  rune = 48
	nineRune  rune = 57
)

func Work(str string) string {

	var prevLetter rune
	var resultStringBuilder strings.Builder
	var needScreen = false

	for _, char := range str {

		currentLetter := char

		// /
		if rune(currentLetter) == slashRune && !needScreen {
			needScreen = true
			continue
		}

		// digidts
		if zeroRune < rune(currentLetter) && rune(currentLetter) < nineRune && !needScreen {

			count, err := strconv.Atoi(string(char))
			if err != nil {
				return ""
			}
			if prevLetter == 0 {
				return ""
			}

			//Один символ уже есть, нам нужно добавить на один меньше вставленного числа
			count--
			resultStringBuilder.WriteString(strings.Repeat(string(prevLetter), count))

		} else {

			prevLetter = rune(currentLetter)
			resultStringBuilder.WriteRune(rune(currentLetter))
		}

		needScreen = false
	}
	return resultStringBuilder.String()
}
